import { PiniaPluginContext } from 'pinia';

interface StorageInterface {
    getItem(key: string): string | null;
    setItem(key: string, value: string): void;
    [name: string]: unknown;
}
interface PersistStrategy {
    key?: string;
    storage?: StorageInterface;
    paths?: string[];
}
interface PersistOptions {
    enabled: true;
    strategies?: PersistStrategy[];
}
declare module 'pinia' {
    interface DefineStoreOptionsBase<S, Store> {
        persist?: PersistOptions;
    }
}
declare const _default: ({ options, store }: PiniaPluginContext) => void;

export { PersistOptions, PersistStrategy, StorageInterface, _default as default };

define(['vue'], (function (vue) { 'use strict';

    const updateStorage = (strategy, store) => {
        const storage = strategy.storage;
        const storeKey = strategy.key || store.$id;
        if (!storage)
            throw new Error('请填写storage');
        if (strategy.paths) {
            const partialState = strategy.paths.reduce((finalObj, key) => {
                finalObj[key] = store.$state[key];
                return finalObj;
            }, {});
            storage.setItem(storeKey, JSON.stringify(partialState));
        }
        else {
            storage.setItem(storeKey, JSON.stringify(store.$state));
        }
    };
    var index = ({ options, store }) => {
        var _a, _b, _c, _d;
        if ((_a = options.persist) === null || _a === void 0 ? void 0 : _a.enabled) {
            const defaultStrat = [
                {
                    key: store.$id,
                },
            ];
            const strategies = ((_c = (_b = options.persist) === null || _b === void 0 ? void 0 : _b.strategies) === null || _c === void 0 ? void 0 : _c.length)
                ? (_d = options.persist) === null || _d === void 0 ? void 0 : _d.strategies
                : defaultStrat;
            strategies.forEach((strategy) => {
                const storage = strategy.storage || sessionStorage;
                const storeKey = strategy.key || store.$id;
                const storageResult = storage.getItem(storeKey);
                if (storageResult) {
                    store.$patch(JSON.parse(storageResult));
                    updateStorage(strategy, store);
                }
            });
            vue.watch(() => store.$state, () => {
                strategies.forEach((strategy) => {
                    updateStorage(strategy, store);
                });
            }, { deep: true });
        }
    };

    return index;

}));

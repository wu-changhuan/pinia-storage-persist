import typescript from '@rollup/plugin-typescript'
import dts from 'rollup-plugin-dts'

export default [
    {
        input: 'src/index.ts',
        output: [
            {
                file: 'dist/index.common.js',
                format: 'cjs', // CommonJS 模块化方案
                globals: {
                    vue: 'vue', // 指明 global.vue 即是外部依赖 vue
                },
            },
            {
                file: 'dist/index.esm.js',
                format: 'esm', // ES6 模块化方案
                globals: {
                    vue: 'vue', // 指明 global.vue 即是外部依赖 vue
                },
            },
            {
                file: 'dist/index.amd.js',
                format: 'amd', // AMD 模块化方案
                globals: {
                    vue: 'vue', // 指明 global.vue 即是外部依赖 vue
                },
            },
            {
                file: 'dist/index.umd.js',
                format: 'umd', // UMD 模块化方案
                name: 'pinia-storage-persist', // UMD 模块化方案需要指定全局变量名称
                globals: {
                    vue: 'vue', // 指明 global.vue 即是外部依赖 vue
                },
            },
        ],
        plugins: [
            typescript({
                sourceMap: false,
            }),
        ],
        external: ['vue'],
    },

    {
        input: 'src/index.ts',
        plugins: [
            dts({
                compilerOptions: {
                    preserveSymlinks: false,
                },
            }),
        ],
        output: {
            format: 'esm',
            file: 'dist/index.d.ts',
        },
    },
]
